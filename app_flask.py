# coding=utf-8
# Created by Ievgen Bryl at 21.03.2024

from __future__ import annotations

from flask import Flask, session
import routes

from libs.funcs import generate_rnd_string


app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = "uploads"
app.config["SESSION_TYPE"] = "cachelib"
app.config["SESSION_PERMANENT"] = True
app.secret_key = "!!! DO NOT USE THIS SECRET KEY !!!"  # TODO: IMPORTANT!!! Change app secret key!


@app.before_request
def before_request_hook() -> None:
    """Set session id if not found during any request."""
    if "id" not in session:
        session["id"] = generate_rnd_string(length=128)


@app.route("/", methods=["GET"])
def index() -> str:
    """Index page render."""
    return routes.index()


if __name__ == "__main__":
    app.run(
        debug=True,  # TODO: change it according to your needs
    )

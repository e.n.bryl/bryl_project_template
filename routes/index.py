# coding=utf-8
# Created by Ievgen Bryl at 22.03.2024

from __future__ import annotations

from flask import render_template


def index() -> str:
    """Index logic and rendering."""
    return render_template("index.html")

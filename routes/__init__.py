# coding=utf-8
# Created by Ievgen Bryl at 25.03.2024

from __future__ import annotations

from routes.index import index

__all__ = ["index"]

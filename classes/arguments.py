# coding=utf-8
# Created by Ievgen Bryl at 25.03.2024

from __future__ import annotations

from typing import NamedTuple


class Arguments(NamedTuple):
    """Class with all arguments passed to script."""

    test_string_argument: str | None
    test_bool_argument: bool

# coding=utf-8
# Created by Ievgen Bryl at 25.03.2024

from __future__ import annotations

import os

from dotenv import load_dotenv

from classes.config import CONFIG
from libs.args import process_arguments
from libs.logging.logger import Logging
from libs.logging.wrappers import PropertyWrapper


def main() -> None:
    """Entry point of project."""
    arguments = process_arguments()
    load_dotenv(dotenv_path=os.path.join(".env", ".env"))

    Logging.prepare_to_output(
        prefix="<w>{0.datetime_now:%H:%M:%S} </w>",
        prefix_args=(PropertyWrapper(),),
    )

    Logging.echo(CONFIG.SETTING_EXAMPLE)
    if arguments.test_string_argument is not None:
        Logging.echo("You passed -test_string_argument: '{message}'".format(message=arguments.test_string_argument))
    else:
        Logging.echo("You didn't pass -test_string_argument argument. It's not an error, it's only app logic :)")
    if arguments.test_bool_argument:
        Logging.echo("You passed -optional test_bool_argument as True")


if __name__ == "__main__":
    main()
